export default function($stateProvider) {
    'ngInject';
    $stateProvider.state('root.categoria', {
        url: "/categoria/:categoria_name",
        views: {
            'content@root': {
                template: require("./categoria.html"),
                controller: "CategoriaController as categoriaCtrl"
            }
        }
    });
}