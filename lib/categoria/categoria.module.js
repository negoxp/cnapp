import modConfig from './categoria.config';
import modController from './categoria.controller';

let mod = angular.module('prototype.categoria', [
    'ionic',
    'ui.router',
    'prototype.constant',
    'wp-api-angularjs'
]);

mod.config(modConfig);
mod.controller('CategoriaController', modController);

export default mod = mod.name
